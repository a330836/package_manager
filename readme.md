# Creacion de un programa con npm
Creacion de mi primer programa con npm y demas paquetes que fueron instalados para su uso y aprendizaje.

## Prerequisitos
Se necesito instalar:
-Git
-Nodejs
-Visual Studio Code

## Corriendo las pruebas
Se dieron las pruebas en donde se imprimian los mensajes de debug, donde teniamos una prueba de una suma de numero para validar que fuera verdad o no, partiendo de la validacion se mostraban los mensajes correspondientes.

## Built With
-Nodejs
-Mocha
-Log4js
-Eslint

## Authors
-Manuel Balderrama

## El funcionamiento de los npm
npm start: se utiliza para  ejecutar el archivo definido 
npm install: instala un paquete o demas paquetes que dependen de.


## Paquetes instalados en el proyecto
mocha: marco de prueba de js que se ejecuta en nodejs, hace que las pruebas asincronas sean simples.
log4js: permite escribir mensajes de registro, cuyo proposito es dejar constancia de una determinada transaccion en tiempo de ejecucion.
eslint: herramienta para identificar y reportar patrones encontrados en codigo JS.

## gitignore
Le dice a git que ignore los archivos o carpetas dentro del .gitignore

## node_modules
Directorio que se crea en la carpeta raiz de nuestro proyecto cuando instalamos pquetes o dependencias mediante npm

## package.json
Archivo generado automaticamente cuando se instalan paquetes o dependencias en el proyecto.
